var BookmarkShortcutsEditor = function () {
	"use strict";

	var isNodeFolder = function (node) {
		return !!node.children;
	};

	function createBookmarkItem(bookmark) {
		var isBookmark = !isNodeFolder(bookmark);

		var li = document.createElement('li');

		var cssClass = 'bookmark';

		if (isBookmark) {
//			li.fontSize = "9"; // = "url('chrome://favicon/'" + bookmark.url + "');";
			li.style.backgroundImage = "url(chrome://favicon/" + bookmark.url + ")";
			li.dataset.bookmarkId = bookmark.id;
		}
		else {
			cssClass = 'folder';
		}

		li.textContent = bookmark.title;
		li.classList.add(cssClass);

		if (!isBookmark) {
			createBookmarksSubtree(bookmark, li);
		}

		return li;
	}

	var createBookmarksSubtree = function (elements, target) {

		var ul = document.createElement('ul');
		for (var i = 0, length = elements.children.length; i < length; i++) {

			var bookmark = elements.children[i];
			var li = createBookmarkItem(bookmark, isNodeFolder);

			ul.appendChild(li);
		}

		target.appendChild(ul);
	};

	var loadBookmarks = function () {
		chrome.bookmarks.getTree(function (data) {

			var bookmarkRoot = data[0];
			var body = document.body;

			createBookmarksSubtree(bookmarkRoot, body);

		});
	};

	console.log('BookmarkKeystroke loaded!');

	return {
		loadBookmarks: loadBookmarks
	}
};

// Run our kitten generation script as soon as the document's DOM is ready.
document.addEventListener('DOMContentLoaded', function () {
	var instance = new BookmarkShortcutsEditor();
	instance.loadBookmarks();
});

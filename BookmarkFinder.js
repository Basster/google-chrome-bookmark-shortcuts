chrome.storage.sync.set({
	BookmarkShortcuts: [
		{
			id: "640",
			shortcut: 'p2w'
		}
	]
});

var BookmarkFinder = function () {
	"use strict";

	var bookmarkShortcuts;

	/**
	 *
	 * @param text
	 * @returns {Array}
	 */
	var findShortcutsByShortcut = function (text) {
		if (bookmarkShortcuts.hasOwnProperty('BookmarkShortcuts')) {
			var matches = [];
			var shortcuts = bookmarkShortcuts['BookmarkShortcuts'];

			for (var idx in shortcuts) {
				if (shortcuts.hasOwnProperty(idx)) {
					var item = shortcuts[idx];
					if (item.shortcut.search(text) >= 0) {
						matches.push(item.id);
					}
				}
			}
		}
		return matches;
	};

	/**
	 *
	 * @param id
	 * @param [field]
	 * @returns {*}
	 */
	var findShortcutByField = function (id, field) {

		field = field || 'id';

		if (bookmarkShortcuts.hasOwnProperty('BookmarkShortcuts')) {
			var shortcuts = bookmarkShortcuts['BookmarkShortcuts'];
			for (var idx in shortcuts) {
				if (shortcuts.hasOwnProperty(idx)) {
					var item = shortcuts[idx];
					if (item[field] === id) {
						return item;
					}
				}
			}
		}
		return null;
	};

	var onInputChangedHandler = function (text, suggest) {
		console.log('inputChanged: ' + text);

		if (bookmarkShortcuts) {
			var shortcutItems = findShortcutsByShortcut(text);
			chrome.bookmarks.get(shortcutItems, function (bookmarks) {
				var suggestions = [];
				for (var idx in bookmarks) {
					if (bookmarks.hasOwnProperty(idx)) {
						var bookmark = bookmarks[idx];
						var shortcut = findShortcutByField(bookmark.id);
						if (shortcut) {
							suggestions.push({
								content: shortcut.shortcut,
								description: '<match>' + shortcut.shortcut + '</match> ' + bookmark.title + ' <url>' + bookmark.url + '</url>'
							});
						}
					}
				}
				suggest(suggestions);
			});
		}
	};

	var onInputStartedHandler = function () {
		console.log('inputStarted!');
		chrome.storage.sync.get('BookmarkShortcuts', function (items) {
			bookmarkShortcuts = items;
		});
	};

	var onInputEnteredHandler = function (text) {
		console.log('inputEntered: ' + text);
		var shortcut = findShortcutByField(text, 'shortcut');
		if (shortcut) {
			chrome.bookmarks.get(shortcut.id.toString(), function (bookmarks) {
				navigate(bookmarks[0].url);
			});
		}
		else {
			alert('Unknown shortcut "' + text + '"!');
		}
	};

	/**
	 * Opens the given url in the currently active tab.
	 *
	 * @param url
	 */
	function navigate(url) {
		chrome.tabs.query({active: true, currentWindow: true}, function(tabs) {
			chrome.tabs.update(tabs[0].id, {url: url});
		});
	}

	return {
		onInputChanged: onInputChangedHandler,
		onInputEntered: onInputEnteredHandler,
		onInputStarted: onInputStartedHandler
	}
};

var bfInstance = new BookmarkFinder();

// This event is fired each time the user updates the text in the omnibox,
// as long as the extension's keyword mode is still active.
chrome.omnibox.onInputChanged.addListener(bfInstance.onInputChanged);

// This event is fired with the user accepts the input in the omnibox.
chrome.omnibox.onInputEntered.addListener(bfInstance.onInputEntered);

chrome.omnibox.onInputStarted.addListener(bfInstance.onInputStarted);